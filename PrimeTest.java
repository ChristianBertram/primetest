import java.math.BigInteger;

public class PrimeTest {
	public static void main(String[] args) {
		BigInteger maxNum = new BigInteger(args[0]);
		for (BigInteger num = new BigInteger("2"); num.compareTo(maxNum) < 0; num = num.add(new BigInteger("1"))) {
			boolean prime1 = true;
			for (BigInteger i =  new BigInteger("2"); i.compareTo(num) < 0; i = i.add(new BigInteger("1"))) {
				if (num.mod(i).compareTo(new BigInteger("0")) == new Integer("0")) {
					prime1 = false;
					
					break;
				}
			}
			
			boolean prime2 = false;
			BigInteger equation = bigPow(new BigInteger("2"), num).subtract(new BigInteger("2"));
			if (equation.mod(num).compareTo(new BigInteger("0")) == 0)
				prime2 = true;
			
			if (prime1 == prime2) 
				System.out.println("true :" + num);
			else 
				System.out.println(num + ": " + prime1 + " " + prime2);
		}
	}
	
	private static BigInteger bigPow(BigInteger a, BigInteger b) {
		BigInteger result = a;
		for (BigInteger i = new BigInteger("1"); i.compareTo(b) < 0; i = i.add(new BigInteger("1"))) {
			result = result.multiply(a);
		}
		return result;
	}
}
